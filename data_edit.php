<?php
require __DIR__ . '/__connect_db.php';
$pname = 'data_edit';
$success = false;

if(isset($_POST['id'])) {

    $sql = sprintf("UPDATE `address_book` SET 
`name`='%s',
`mobile`='%s',
`email`='%s',
`birthday`='%s',
`address`='%s' 
WHERE `id`='%s'",
        $mysqli->escape_string($_POST['name']),
        $mysqli->escape_string($_POST['mobile']),
        $mysqli->escape_string($_POST['email']),
        $mysqli->escape_string($_POST['birthday']),
        $mysqli->escape_string($_POST['address']),
        intval($_POST['id'])
    );

    $success = $mysqli->query($sql);

    if($success) {
        $_SESSION['my_msg'] = array(
                'type' => 'success',
                'content' => '編號 '. intval($_POST['id']). ' 的資料修改完成'
        );

        if(isset($_SESSION['come_from'])){
            header('Location: '. $_SESSION['come_from'] );
            unset($_SESSION['come_from']);
        } else {
            header('Location: data_list.php');
        }
        exit;
    }

} else {
    $_SESSION['come_from'] = $_SERVER['HTTP_REFERER'];
}

$id = isset($_GET['id']) ? intval($_GET['id']) : 0;

$sql = "SELECT * FROM `address_book` WHERE `id`=$id";
$rs = $mysqli->query($sql);
if(! $rs->num_rows){
    header("Location: data_list.php");
    exit;
}

$row = $rs->fetch_assoc();




?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">

    <title>Document</title>
</head>
<body>
<div class="container">
    <?php include __DIR__ . '/__navbar.php'; ?>

    <?php //echo $_SERVER['HTTP_REFERER'] ?>

    <?php /* if($success): ?>
    <div class="col-md-12">
        <div class="alert alert-success" role="alert">
            修改資料完成
        </div>
    </div>
    <?php endif; */ ?>
    <div class="col-md-6">

        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">修改資料</h3></div>
                <div class="panel-body">

                    <form method="post">
                        <input type="hidden" name="id" value="<?= $row['id'] ?>">
                        <div class="form-group">
                            <label for="name">姓名</label>
                            <input type="text" class="form-control" id="name" name="name" value="<?= $row['name'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="mobile">手機</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" value="<?= $row['mobile'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="email">電郵</label>
                            <input type="text" class="form-control" id="email" name="email" value="<?= $row['email'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday" value="<?= $row['birthday'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="address">地址</label>
                            <input type="text" class="form-control" id="address" name="address" value="<?= $row['address'] ?>">
                        </div>

                        <button type="submit" class="btn btn-default">修改</button>
                    </form>


                </div>
            </div>

        </div>


    </div>


</div>
<table>

</table>

<script src="lib/jquery-3.1.1.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
</body>
</html>