<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
$a = 123;

define('MY_CONST', 434.546456);

echo $a;
echo '<br>';

echo __DIR__;
echo '<br>';

echo __FILE__;
echo '<br>';


echo __LINE__;
echo '<br>';

echo MY_CONST;
echo '<br>';

?>

</body>
</html>