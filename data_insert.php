<?php
require __DIR__ . '/__connect_db.php';
$pname = 'data_insert';
$success = false;

if(isset($_POST['name'])) {
    echo '<!-- <pre>';
    print_r($_POST);
    echo '</pre> -->';

    $sql = sprintf("INSERT INTO `address_book`(
`id`, `name`, `mobile`, 
`email`, `birthday`, `address`) VALUES (NULL, '%s', '%s', '%s', '%s', '%s' )",
        $mysqli->escape_string($_POST['name']),
        $mysqli->escape_string($_POST['mobile']),
        $mysqli->escape_string($_POST['email']),
        $mysqli->escape_string($_POST['birthday']),
        $mysqli->escape_string(strip_tags($_POST['address']))
    );
    //echo $sql. '---';
    $success = $mysqli->query($sql);
    //exit; //die();
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">

    <title>Document</title>
</head>
<body>
<div class="container">
    <?php include __DIR__ . '/__navbar.php'; ?>

    <?php if($success): ?>
    <div class="col-md-12">
        <div class="alert alert-success" role="alert">
            新增資料完成
        </div>
    </div>
    <?php endif; ?>
    <div class="col-md-6">

        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">新增資料</h3></div>
                <div class="panel-body">

                    <form method="post">
                        <div class="form-group">
                            <label for="name">姓名</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="mobile">手機</label>
                            <input type="text" class="form-control" id="mobile" name="mobile">
                        </div>
                        <div class="form-group">
                            <label for="email">電郵</label>
                            <input type="text" class="form-control" id="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday">
                        </div>
                        <div class="form-group">
                            <label for="address">地址</label>
                            <input type="text" class="form-control" id="address" name="address">
                        </div>

                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>


                </div>
            </div>

        </div>


    </div>


</div>
<table>

</table>

<script src="lib/jquery-3.1.1.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
</body>
</html>