<?php
require __DIR__ . '/__connect_db.php';
$pname = 'data_list';

$per_page = 5;

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;


$r_total = $mysqli->query("SELECT COUNT(1) FROM `address_book`");
$r_row = $r_total->fetch_row();
$total = $r_row[0];
$total_pages = ceil($total/$per_page);

$sql = sprintf("SELECT * FROM `address_book` ORDER BY `id` DESC LIMIT %s, %s",
    ($page-1)*$per_page,
    $per_page
    );


$result = $mysqli->query($sql);


//$result = $mysqli->query("SELECT * FROM `address_book` ORDER BY `name` DESC, `id` DESC");

// SELECT 1 FROM `address_book`
// SELECT COUNT(1) FROM `address_book`
// SELECT COUNT(*) FROM `address_book`


// SELECT * FROM `address_book` ORDER BY `id` DESC LIMIT 0, 5
// SELECT * FROM `address_book` ORDER BY `id` DESC LIMIT 5, 5
// SELECT * FROM `address_book` ORDER BY `id` DESC LIMIT 10, 5



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
    <script src="lib/jquery-3.1.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <title>Document</title>
    <style>
        .edit {
            color: blue;
            font-size: larger;
        }
        .delete {
            color: #ff2118;
            font-size: larger;
        }
    </style>
</head>
<body>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

    <?php  if(isset($_SESSION['my_msg'])): ?>
    <div class="col-md-12" id="my_msg">
        <div class="alert alert-<?= $_SESSION['my_msg']['type'] ?>" role="alert">
            <?= $_SESSION['my_msg']['content'] ?>
        </div>
    </div>
    <script>
        $(function(){
            setTimeout(function(){
                $('#my_msg').slideUp();
            }, 3000);
        });
    </script>
    <?php
    unset($_SESSION['my_msg']);
    endif;  ?>


    <div class="col-md-12">
        <nav>
            <ul class="pager">
                <?php if($page>1): ?>
                    <li><a href="data_list.php?page=1">First</a></li>
                    <li><a href="data_list.php?page=<?= $page-1 ?>"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                <?php else: ?>
                    <li class="disabled"><a>First</a></li>
                    <li class="disabled"><a><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                <?php endif; ?>
                <li class="disabled"><a><?= $page. ' / '. $total_pages ?></a></li>

                <?php if($page < $total_pages): ?>
                    <li><a href="data_list.php?page=<?= $page+1 ?>"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                    <li><a href="data_list.php?page=<?= $total_pages ?>">Last</a></li>
                <?php else: ?>
                    <li class="disabled"><a><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                    <li class="disabled"><a>Last</a></li>
                <?php endif; ?>
            </ul>
        </nav>
    </div>

    <div class="col-md-12">
        <div class="row">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>del</th>
                <th>#</th>
                <th>Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Birthday</th>
                <th>Address</th>
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>
            <?php while ($row = $result->fetch_assoc()): ?>
                <tr>
                    <td><a href="javascript: delete_it(<?= $row['id'] ?>)">
                            <span class="glyphicon glyphicon-remove-sign delete" aria-hidden="true"></span>
                        </a></td>
                    <td><?= $row['id'] ?></td>
                    <td><?= $row['name'] ?></td>
                    <td><?= $row['mobile'] ?></td>
                    <td><?= $row['email'] ?></td>
                    <td><?= $row['birthday'] ?></td>
                    <td><?= htmlentities($row['address']) ?></td>
                    <td><a href="data_edit.php?id=<?= $row['id'] ?>">
                        <span class="glyphicon glyphicon-pencil edit" aria-hidden="true"></span>
                        </a></td>
                </tr>
            <?php endwhile; ?>

            </tbody>
        </table>
        </div>

    </div>


</div>

<script>
    function delete_it(id){
        if(confirm('您確定要刪除編號為 ' + id + ' 的資料嗎?')){
            location.href = 'data_delete.php?id=' + id;

        }
    }

</script>

</body>
</html>