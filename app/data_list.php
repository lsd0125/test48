<?php
require __DIR__ . '/__connect_db.php';

$page_name = 'data_list';

// 總共有幾筆，共有幾頁，每一頁有幾筆
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
if ($page < 1) {
    $page = 1;
}
$per_page = 5;


$result_total = $mysqli->query("SELECT COUNT(1) FROM `address_book`");
$row_total = $result_total->fetch_array();
$total = $row_total[0];
// print_r($row_total);

$pages = ceil($total / $per_page);

$sql = sprintf("SELECT * FROM `address_book` ORDER BY `sid` DESC LIMIT %s, %s",
    ($page - 1) * $per_page,
    $per_page
);


$result = $mysqli->query($sql);

//$row = $result->fetch_assoc(); //讀取一筆資料
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
    <script src="lib/jquery-3.1.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
</head>
<body>

<div class="container">
    <?php include __DIR__ . '/__navbar.php' ?>


    <div class="row">
        <div class="col-sm-12">
            <nav aria-label="...">
                <ul class="pagination">
                    <?php for ($i = 1; $i <= $pages; $i++) { ?>
                    <li class="<?= $i==$page ? 'active' : '' ?>">
                        <a href="?page=<?=$i?>"><?= $i ?></a>
                    </li>
                    <?php } ?>
                </ul>
            </nav>
        </div>
    </div>


    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><span class="glyphicon glyphicon-trash"></span></th>
            <th>#</th>
            <th>name</th>
            <th>email</th>
            <th>mobile</th>
            <th>birthday</th>
            <th>address</th>
            <th><span class="glyphicon glyphicon-pencil"></span></th>
        </tr>
        </thead>
        <tbody>
        <?php while ($row = $result->fetch_assoc()): ?>
            <tr>
                <td>
                    <a href="javascript:delete_it(<?= $row['sid'] ?>)">
                    <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
                <td><?= $row['sid'] ?></td>
                <td><?= $row['name'] ?></td>
                <td><?= $row['email'] ?></td>
                <td><?= $row['mobile'] ?></td>
                <td><?= $row['birthday'] ?></td>
                <!-- PHP: strip_tags()  -->
                <td><?= htmlentities($row['address']) ?></td>
                <td>
                    <a href="data_edit.php?sid=<?= $row['sid'] ?>">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                </td>
            </tr>
        <?php endwhile; ?>
        </tbody>
    </table>


</div>

<script>
    function delete_it(sid){
        //alert(sid);
        if( confirm("確定要刪除編號為 "+ sid +" 的資料嗎？") ){
            location.href = "data_delete.php?sid=" + sid;
        }

    }

</script>

</body>
</html>