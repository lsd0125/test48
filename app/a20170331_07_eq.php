<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<pre>
<?php
$ar = array(
        'name' => 'peter',
        'age' => 26,
        'gender' => 'male',

);

$br = $ar;
$cr = &$ar;

$br['name'] = 'bill';
$cr['name'] = 'carl';


print_r($ar);
print_r($br);
print_r($cr);
?>
    </pre>
</body>
</html>