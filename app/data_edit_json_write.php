<?php
require __DIR__ . '/__connect_db.php';

$result = [
    'success' => false,
    'error' => '沒有 post 資料',
    'data' => '',
];

if(isset($_POST['name']) and isset($_POST['sid'])) {
    $result['error'] = '';
    $sql = "UPDATE `address_book` 
SET `name`=?,`email`=?,`mobile`=?,`birthday`=?,`address`=?
WHERE `sid`=?";
    $stmt = $mysqli->prepare($sql);

    //錯誤處理
    if($stmt->errno){
        $result['error'] = $stmt->error;
        echo json_encode($result, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        exit;
    }

    $stmt->bind_param('sssssi',
        $_POST['name'],
        $_POST['email'],
        $_POST['mobile'],
        $_POST['birthday'],
        $_POST['address'],
        $_POST['sid']
        );

    $stmt->execute();
    $af = $stmt->affected_rows;
    if($af==1){
        $result['success'] = true;
    }
    $result['data'] = $_POST;
}

echo json_encode($result, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);