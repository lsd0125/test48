<?php
require __DIR__ . '/__connect_db.php';

$result = [
    'success' => false,
    'error' => '沒有給 sid 參數',
    'affected' => 0
];

if(isset($_GET['sid'])){

    $result['error'] = '';
    $result['sid'] = $_GET['sid'];

    $sid = intval($_GET['sid']);

    $sql = "DELETE FROM `address_book` WHERE `sid`=?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param('i', $sid);

    $stmt->execute();
    if($stmt->affected_rows){
        $result['affected'] = $stmt->affected_rows;
        $result['success'] = true;
    }
    $stmt->close();

}

echo json_encode($result, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
