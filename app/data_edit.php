<?php
require __DIR__ . '/__connect_db.php';

$page_name = 'data_edit';

$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;



if(isset($_POST['name'])) {
    $sql = "UPDATE `address_book` 
SET `name`=?,`email`=?,`mobile`=?,`birthday`=?,`address`=?
WHERE `sid`=?";
    $stmt = $mysqli->prepare($sql);

    //錯誤處理
    if($stmt->errno){
        echo $stmt->error;
        exit;
    }

    $stmt->bind_param('sssssi',
        $_POST['name'],
        $_POST['email'],
        $_POST['mobile'],
        $_POST['birthday'],
        $_POST['address'],
        $sid
        );

    $stmt->execute();
    //header('Location: data_list.php');
    $af = $stmt->affected_rows;
    //exit;
}

$rs = $mysqli->query("SELECT * FROM `address_book` WHERE `sid`=$sid");
// 如果沒有找到資料，轉到列表頁
if($rs->num_rows<1){
    header("Location: data_list.php");
    exit; //die();
}
$row = $rs->fetch_assoc();


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
    <script src="lib/jquery-3.1.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
</head>
<body>

<div class="container">
    <?php include __DIR__ . '/__navbar.php' ?>

    <?php if(isset($af) and $af==1): ?>
    <div class="col-sm-12">
        <div class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            修改完成
        </div>
    </div>
    <?php endif ?>

    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">修改資料</h3></div>
                <div class="panel-body">
                    <form name="form1" method="post" onsubmit="return checkForm();">
                        <div class="form-group">
                            <label for="name">姓名</label>
                            <input type="text" class="form-control"
                                   value="<?= $row['name'] ?>"
                                   id="name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="email">email</label>
                            <input type="text" class="form-control"
                                   value="<?= $row['email'] ?>"
                                   id="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="mobile">手機號碼</label>
                            <input type="text" class="form-control"
                                   value="<?= $row['mobile'] ?>"
                                   id="mobile" name="mobile">
                        </div>
                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control"
                                   value="<?= $row['birthday'] ?>"
                                   id="birthday" name="birthday">
                        </div>
                        <div class="form-group">
                            <label for="address">地址</label>
                            <input type="text" class="form-control"
                                   value="<?= $row['address'] ?>"
                                   id="address" name="address">
                        </div>

                        <button type="submit" class="btn btn-default">修改</button>
                    </form>
                </div>
            </div>



        </div>
    </div>

</div>
<script>

function checkForm(){

    if(document.form1.name.value.length<2){
        alert('請填寫正確姓名');
        return false;
    }


    if(! validateEmail(document.form1.email.value)){
        alert('email 格式不正確');
        return false;
    }




}
// http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


</script>
</body>
</html>