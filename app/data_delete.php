<?php
require __DIR__ . '/__connect_db.php';

if(isset($_GET['sid'])){
    $sid = intval($_GET['sid']);

    $sql = "DELETE FROM `address_book` WHERE `sid`=?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param('i', $sid);

    $stmt->execute();

    // echo $stmt->affected_rows;
    $stmt->close();

}
if(! empty($_SERVER['HTTP_REFERER'])){

    header("Location: ". $_SERVER['HTTP_REFERER']);
} else {
    header("Location: data_list.php");
}
