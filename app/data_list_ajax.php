<?php
require __DIR__ . '/__connect_db.php';

$page_name = 'data_list';

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
    <script src="lib/jquery-3.1.1.js"></script>
    <script src="lib/underscore.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
</head>
<body>

<script type="text/x-template" id="item_tpl">
    <tr>
        <td>
            <a href="javascript:delete_it(<%= sid %>)">
                <span class="glyphicon glyphicon-trash"></span>
            </a>
        </td>
        <td><%= sid %></td>
        <td><%= name %></td>
        <td><%= email %></td>
        <td><%= mobile %></td>
        <td><%= birthday %></td>
        <td><%= address %></td>
        <td>
            <a href="data_edit.php?sid=<%= sid %>">
                <span class="glyphicon glyphicon-pencil"></span>
            </a>
        </td>
    </tr>
</script>
<script type="text/x-template" id="pagi_tpl">
    <li class="<%= active %>">
        <a href="#<%= i %>"><%= i %></a>
    </li>
</script>


<div class="container">
    <?php include __DIR__ . '/__navbar.php' ?>

    <div class="row">
        <div class="col-sm-12">
            <nav aria-label="...">
                <ul class="pagination">
                </ul>
            </nav>
        </div>
    </div>

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><span class="glyphicon glyphicon-trash"></span></th>
            <th>#</th>
            <th>name</th>
            <th>email</th>
            <th>mobile</th>
            <th>birthday</th>
            <th>address</th>
            <th><span class="glyphicon glyphicon-pencil"></span></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>


</div>

<script>
    function delete_it(sid){
        //alert(sid);
        if( confirm("確定要刪除編號為 "+ sid +" 的資料嗎？") ){
            location.href = "data_delete.php?sid=" + sid;
        }

    }
    var tpl_str = $('#item_tpl').text();
    var tpl_fun = _.template(tpl_str);
    var pag_str = $('#pagi_tpl').text();
    var pag_fun = _.template(pag_str);
    var page = 1;

    var loadData = function(){

        $.get('data_list_json.php', {page:page}, function(data){
            var i, active;
            //console.log(data);
            $('tbody').empty();
            for(i=0; i< data.data.length; i++) {
                $('tbody').append(tpl_fun(data.data[i]));
            }
            $('.pagination').empty();
            for(i=1; i<= data.pages_num; i++) {
                active = i==data.page ? 'active' : '';
                $('.pagination').append(pag_fun({
                    active: active, i: i
                }));
            }

        }, 'json');
    };

    window.addEventListener('hashchange', function(){
        var hash = location.hash;
        page = parseInt(hash.slice(1));

        page < 1 ? (page=1) : '';
        loadData();
    });

    $(function(){
        var hash = location.hash;
        page = parseInt(hash.slice(1));

        page < 1 ? (page=1) : '';
        loadData();
    });

</script>

</body>
</html>