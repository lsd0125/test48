<?php
require __DIR__ . '/__connect_db.php';

$per_page = 5; // 一頁顯示幾筆
$result = array(
    'success' => false,
    'per_page' => $per_page,
);

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
if ($page < 1) {
    $page = 1;
}
$result['page'] = $page; // 顯示第幾頁


$result_total = $mysqli->query("SELECT COUNT(1) FROM `address_book`");
$row_total = $result_total->fetch_array();
$total = $row_total[0];
$result['items_num'] = $total; // 總筆數


$pages = ceil($total / $per_page);
$result['pages_num'] = $pages; // 總頁數

$sql = sprintf("SELECT * FROM `address_book` ORDER BY `sid` DESC LIMIT %s, %s",
    ($page - 1) * $per_page,
    $per_page
);


$rs = $mysqli->query($sql);
$data = array(); // 資料
while ($row = $rs->fetch_assoc()){
    $data[] = $row;
}

$result['data'] = $data;
$result['success'] = true;

echo json_encode($result, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);


