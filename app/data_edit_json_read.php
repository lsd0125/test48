<?php
require __DIR__ . '/__connect_db.php';

$result = [
    'success' => false,
    'error' => '沒有給 sid 參數',
    'data' => 0
];


$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;
if($sid>0) {

    $result['error'] = '';

    $rs = $mysqli->query("SELECT * FROM `address_book` WHERE `sid`=$sid");
    if($rs->num_rows<1){
        $result['error'] = '找不到該筆資料';
    } else{
        $row = $rs->fetch_assoc();
        $result['data'] = $row;
        $result['success'] = true;
    }
}

echo json_encode($result, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
