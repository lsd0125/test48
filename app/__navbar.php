<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false"><span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Brand</a></div>
        <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1" aria-expanded="false"
             style="height: 1px;">
            <ul class="nav navbar-nav">
                <li class="<?= $page_name=='data_list' ? 'active' : '' ?>">
                    <a href="data_list.php">資料列表</a>
                </li>
                <li class="<?= $page_name=='data_insert' ? 'active' : '' ?>">
                    <a href="data_insert.php">新增資料</a>
                </li>

            </ul>

        </div>
    </div>
</nav>