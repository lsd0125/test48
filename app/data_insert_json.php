<?php
require __DIR__ . '/__connect_db.php';

$result = [
        'success' => false,
        'error' => '沒有 post 資料',
        'data' => '',
];

if(isset($_POST['name'])) {
    $result['error'] = '';
    $sql = "INSERT INTO `address_book`(
        `sid`, `name`, `email`, `mobile`, `birthday`, `address`
        ) VALUES (
        NULL, ?, ?, ?, ?, ?)";
    $stmt = $mysqli->prepare($sql);

    //錯誤處理
    if($stmt->errno){
        $result['error'] =  $stmt->error;
    }

    $stmt->bind_param('sssss',
        $_POST['name'],
        $_POST['email'],
        $_POST['mobile'],
        $_POST['birthday'],
        $_POST['address']
        );

    $stmt->execute();

    if($stmt->affected_rows){
        $result['success'] = true;
    }

    $result['data'] = $_POST;
}

echo json_encode($result, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);

