-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2017-03-16 04:25:15
-- 伺服器版本: 10.1.21-MariaDB
-- PHP 版本： 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- 資料庫： `mytest`
--

-- --------------------------------------------------------

--
-- 資料表結構 `address_book`
--

CREATE TABLE `address_book` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `birthday` datetime NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `address_book`
--

INSERT INTO `address_book` (`id`, `name`, `mobile`, `email`, `birthday`, `address`) VALUES
(1, '李大一', '0918333444', 'sdfs@sefsde.com', '2017-01-03 07:00:00', '台北市'),
(3, '李大一', '0918333444', 'sdfs@sefsde.com', '2017-01-03 07:00:00', '台北市'),
(4, '李大一', '0935777888', 'sdfsd@jdfgs.com', '2017-03-01 00:00:00', '台中市'),
(5, 'dfvsd', '0935111000', '', '0000-00-00 00:00:00', ''),
(6, '黃小華', '0918', 'safasd@sdfs.com', '1990-05-05 00:00:00', '宜蘭縣'),
(7, 'dfgbdsfgdfgd', 'dsfgdsfg', 'dfgdf', '0000-00-00 00:00:00', 'dsfg'),
(8, 'sdfghsd', 'ddrfgd', 'dgdf', '0000-00-00 00:00:00', 'dfgd'),
(9, 'drfgdfvgd', 'sdf', 'sdf', '0000-00-00 00:00:00', '<script>alert('),
(10, '李大一', '0918333444', 'sdfs@sefsde.com', '2017-01-03 07:00:00', '台北市'),
(11, '李大一', '0935777888', 'sdfsd@jdfgs.com', '2017-03-01 00:00:00', '台中市'),
(12, '李大一', '0918333444', 'sdfs@sefsde.com', '2017-01-03 07:00:00', '台北市'),
(14, 'dfvsd', '0918123', 'ewre@dgf.com', '0000-00-00 00:00:00', '台東縣太麻里'),
(15, '黃中華', '0918123', 'safasd@sdfs.com', '1990-09-05 00:00:00', '宜蘭縣'),
(18, 'drfg', 'sdf', 'sdf', '0000-00-00 00:00:00', '<script>alert(\"爛芭樂\")</script>');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `address_book`
--
ALTER TABLE `address_book`
  ADD PRIMARY KEY (`id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `address_book`
--
ALTER TABLE `address_book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;